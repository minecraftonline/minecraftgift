package com.minecraftonline.minecraftgift.potionlists;

import com.minecraftonline.minecraftgift.WeightedItemDeterminer;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Color;

import java.util.Arrays;
import java.util.Calendar;


public class ChristmasPotionList extends WeightedItemDeterminer {

    private static final String GIFT_LIST_NAME = "Christmas";

    public String giftListName() {
        return GIFT_LIST_NAME;
    }

    public ChristmasPotionList() {
        log.info("Initializing the " + giftListName() + " potion list");

        int year = Calendar.getInstance().get(Calendar.YEAR);

        add(ItemStack.builder()
                .itemType(ItemTypes.POTION)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, "Eggnog"))
                .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "Christmas ", year)))
                //
                .add(Keys.POTION_EFFECTS,
                        Arrays.asList(
                                PotionEffect.of(PotionEffectTypes.NAUSEA, 0, 1200),
                                PotionEffect.of(PotionEffectTypes.MINING_FATIGUE, 1, 1200),
                                PotionEffect.of(PotionEffectTypes.SPEED, 2, 1800)
                        ))
                // Is this how to color?
                .add(Keys.POTION_COLOR, Color.YELLOW)
                .build(), DEFAULT_WEIGHT * 4);
    }
}

