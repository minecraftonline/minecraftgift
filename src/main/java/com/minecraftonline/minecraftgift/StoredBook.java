package com.minecraftonline.minecraftgift;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.spongepowered.api.asset.Asset;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StoredBook {

    private static final String PAGES_KEY = "pages";
    private static final String TITLE_KEY = "title";
    private static final String AUTHOR_KEY = "author";
    private static final String GENERATION_KEY = "generation";
    private static final String DISPLAY_KEY = "display";
    private static final String LORE_KEY = "Lore";

    // Almost solely tyh code

    private ItemStack book = null;
    private final String assetName;

    // Read from JSON at relative path
    public StoredBook(String assetName) {
        this.assetName = assetName;
    }

    public void load(final PluginContainer pluginContainer) throws IOException {
        final Logger log = MinecraftGift.getLogger();
        Optional<Asset> asset = pluginContainer.getAsset(assetName);
        if (!asset.isPresent()) {
            log.error("Failed to locate asset: '" + assetName + "', expected something");
            return;
        }
        final String json = asset.get().readString();
        final JsonParser parser = new JsonParser();
        final JsonElement mainJson = parser.parse(json);
        if (!mainJson.isJsonObject()) {
            log.error("Failed to load asset: '" + assetName + "', expected JsonObject, got: " + mainJson.getClass().getSimpleName());
            return;
        }
        JsonObject mainJsonObj = mainJson.getAsJsonObject();

        final ItemStack book = ItemStack.of(ItemTypes.WRITTEN_BOOK);

        if (!mainJsonObj.has(TITLE_KEY)) {
            log.error("Failed to load book asset: '" + assetName + "', expected a title, found none");
            return;
        }

        JsonElement titleElement = mainJsonObj.get(TITLE_KEY);
        if (!titleElement.isJsonPrimitive()) {
            log.error("Failed to load book asset: '" + assetName + "', expected title to be json primitive, got: " + titleElement.getClass().getSimpleName());
            return;
        }
        book.offer(Keys.DISPLAY_NAME, TextSerializers.PLAIN.deserialize(titleElement.getAsString()));

        if (!mainJsonObj.has(PAGES_KEY) || !mainJsonObj.get(PAGES_KEY).isJsonArray()) {
            log.error("Failed to load asset: '" + assetName + "', expected pages array.");
            return;
        }
        List<Text> pages = new ArrayList<>();
        for (JsonElement item : mainJsonObj.get(PAGES_KEY).getAsJsonArray()) {
            Text.Builder builder = Text.builder();
            final String unescaped = unescapeString(item.toString());
            //System.out.println("Unescaped: " + unescaped);
            JsonElement partElement = parser.parse(unescaped);
            //System.out.println(partElement);
            if (partElement.isJsonObject()) {
                builder.append(TextSerializers.JSON.deserialize(partElement.toString()));
            }
            else if (partElement.isJsonArray()) {
                partElement.getAsJsonArray().forEach(i -> builder.append(TextSerializers.JSON.deserialize(i.toString())));
            }
            else {
                log.error("Failed to load asset: '" + assetName + "', expected data contained in pages to be an object or an array.");
                return;
            }
            pages.add(builder.build());
        }
        book.offer(Keys.BOOK_PAGES, pages);

        if (mainJsonObj.has(AUTHOR_KEY)) {
            final JsonElement authorElement = mainJsonObj.get(AUTHOR_KEY);
            if (!authorElement.isJsonPrimitive()) {
                log.error("Failed to load asset: '" + assetName + "', expected author to be a json primitive, got: " + authorElement.getClass().getSimpleName());
                return;
            }
            final String author = authorElement.getAsString();
            book.offer(Keys.BOOK_AUTHOR, Text.of(author));
        }

        if (mainJsonObj.has(GENERATION_KEY)) {
            final JsonElement generationElement = mainJsonObj.get(GENERATION_KEY);
            if (!generationElement.isJsonPrimitive() || !generationElement.getAsJsonPrimitive().isNumber()) {
                log.error("Failed to load asset: '" + assetName + "', expected generation to be a number, got: " + generationElement.getClass().getSimpleName());
                return;
            }
            int generation = generationElement.getAsInt();
            book.offer(Keys.GENERATION, generation);
        }

        if (mainJsonObj.has(DISPLAY_KEY)) {
            JsonElement displayElement = mainJsonObj.get(DISPLAY_KEY);
            if (displayElement.isJsonObject()) {
                final JsonObject displayObj = displayElement.getAsJsonObject();
                if (displayObj.has(LORE_KEY)) {
                    JsonElement loreElement = displayObj.get(LORE_KEY);
                    if (!loreElement.isJsonArray()) {
                        log.error("Failed to load asset: '" + assetName + "', expected lore to be an array, got: " + loreElement.getClass().getSimpleName());
                        return;
                    }
                    JsonArray loreArray = loreElement.getAsJsonArray();
                    List<Text> lore = new ArrayList<>();
                    for (JsonElement element : loreArray) {
                        if (element.isJsonPrimitive()) {
                            lore.add(Text.of(element.getAsJsonPrimitive().getAsString()));
                        }
                        else if (element.isJsonObject()) {
                            lore.add(TextSerializers.JSON.deserialize(element.toString()));
                        }
                    }
                    book.offer(Keys.ITEM_LORE, lore);
                }
            }
        }

        this.book = book;
        log.info("Loaded asset '" + assetName + "', expected this to happen.");
    }

    public Optional<ItemStack> getBook() {
        if (this.book != null) {
            return Optional.of(this.book.copy());
        }
        return Optional.empty();
    }

    // Ted does not like seeing tyh complaining in variable names like this.
    // It scares him.
    public String unescapeString(String whatIsMojangDoing) {
        final String whyWouldYouPutQuotedJsonInsideJson = whatIsMojangDoing.substring(1, whatIsMojangDoing.length() - 1);
        final String nowIHaveToUnescapeIt = whyWouldYouPutQuotedJsonInsideJson.replace("\\\"", "\"");
        final String andMinecraftItselfNeedsSomeEscapesWhichGetDuplicated = nowIHaveToUnescapeIt.replace("\\\\", "\\");
        return andMinecraftItselfNeedsSomeEscapesWhichGetDuplicated;
    }
}
