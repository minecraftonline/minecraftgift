package com.minecraftonline.minecraftgift;

import com.minecraftonline.minecraftgift.arrowlists.DefaultArrowList;
import com.minecraftonline.minecraftgift.arrowlists.ValentinesArrowList;
import com.minecraftonline.minecraftgift.giftlists.*;
import com.minecraftonline.minecraftgift.potionlists.ChristmasPotionList;
import com.minecraftonline.minecraftgift.potionlists.DefaultPotionList;
import com.minecraftonline.minecraftgift.potionlists.ValentinesPotionList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GiftLists {


    // This was a mess, i clean.
    // I still don't quite like how this works as its possible to screw up fairly easily,
    // but its better than what was here before.
    public static final Map<String, ItemDeterminer> GIFT_LIST_CHOICES = new HashMap<>();

    public static final Map<ItemDeterminer, ItemDeterminer> POTION_LIST_CHOICES = new HashMap<>();
    public static final Map<ItemDeterminer, ItemDeterminer> ARROW_LIST_CHOICES = new HashMap<>();

    /// Main Lists ///
    public static final WeightedItemDeterminer PRIDE = new PrideGiftList();
    public static final WeightedItemDeterminer DEFAULT = new DefaultGiftList();
    public static final WeightedItemDeterminer BIRTHDAY = new BirthdayGiftList();
    public static final WeightedItemDeterminer HALLOWEEN = new HalloweenGiftList();
    public static final WeightedItemDeterminer CHRISTMAS = new ChristmasGiftList();
    public static final WeightedItemDeterminer VALENTINES = new ValentinesGiftList();
    public static final WeightedItemDeterminer ARROW = new ArrowGiftList();
    public static final WeightedItemDeterminer TERRAIN = new TerrainGiftList();


    // Potions //
    public static final WeightedItemDeterminer DEFAULT_POTION = new DefaultPotionList();
    public static final WeightedItemDeterminer CHRISTMAS_POTION = new ChristmasPotionList();

    // Arrow //
    public static final WeightedItemDeterminer DEFAULT_ARROW = new DefaultArrowList();
    public static final WeightedItemDeterminer VALENTINES_ARROW = new ValentinesArrowList();
    public static final WeightedItemDeterminer VALENTINES_POTION = new ValentinesPotionList();
    // :frog:

    /*
    public static final WeightedItemDeterminer VALENTINES = new ValentinesGiftList();
    public static final WeightedItemDeterminer EASTER = new EasterGiftList();
    */

    public static final ArrayList<ItemDeterminer> ALL_GIFT_LISTS = new ArrayList<>();

    static
    {
        GIFT_LIST_CHOICES.put("Pride",PRIDE);
        GIFT_LIST_CHOICES.put("Default", DEFAULT);
        GIFT_LIST_CHOICES.put("Birthday", BIRTHDAY);
        GIFT_LIST_CHOICES.put("Halloween", HALLOWEEN);
        GIFT_LIST_CHOICES.put("Christmas", CHRISTMAS);
        GIFT_LIST_CHOICES.put("Arrow", ARROW);
        GIFT_LIST_CHOICES.put("ValentinesArrow", VALENTINES_ARROW);
        GIFT_LIST_CHOICES.put("Valentines", VALENTINES);
        GIFT_LIST_CHOICES.put("Terrain", TERRAIN);


        ARROW_LIST_CHOICES.put(VALENTINES, VALENTINES_ARROW);
        ARROW_LIST_CHOICES.put(DEFAULT, DEFAULT_ARROW);

        POTION_LIST_CHOICES.put(VALENTINES, VALENTINES_POTION);
        POTION_LIST_CHOICES.put(DEFAULT, DEFAULT_POTION);
        POTION_LIST_CHOICES.put(CHRISTMAS, CHRISTMAS_POTION);



        // CHOICES.put("Valentines",VALENTINES);
        // CHOICES.put("Easter",EASTER);

        // So I can refer to them to get their probabilities
        for (String k : GIFT_LIST_CHOICES.keySet()) {
            // MinecraftGift.getLogger().info("Initializing with " + k);
            ALL_GIFT_LISTS.add(GIFT_LIST_CHOICES.get(k));
        }
    }

    public static ItemDeterminer getPotionGiftList(ItemDeterminer giftList) {
        return POTION_LIST_CHOICES.get(giftList);
    }

    public static ItemDeterminer getArrowGiftList(ItemDeterminer giftList) {
        return ARROW_LIST_CHOICES.get(giftList);
    }
}
