package com.minecraftonline.minecraftgift;

import org.slf4j.Logger;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;

public interface ItemDeterminer
{
    Logger log = MinecraftGift.getLogger();

    // These will need to be overridden, I believe
    String GIFT_LIST_NAME = "";
    default String giftListName() { return GIFT_LIST_NAME; };

    Text GIFT_HANDOUT_TEXT = Text.builder().build();
    default Text giftHandoutText() { return GIFT_HANDOUT_TEXT; };

    int SPECIAL_MAP_ID = -1;
    default int specialMapID() { return SPECIAL_MAP_ID; };

    ItemStack itemToGive();
    // This will work, right?
}
