package com.minecraftonline.minecraftgift;

public class GiftTextUtils {
    public static String capitaliseFirstLetter(String inputString) {
        return Character.toUpperCase(inputString.charAt(0)) + inputString.substring(1);
    }
}
