package com.minecraftonline.minecraftgift.arrowlists;

import com.minecraftonline.minecraftgift.WeightedItemDeterminer;
import com.minecraftonline.minecraftgift.giftlists.ValentinesGiftList;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.potion.PotionType;
import org.spongepowered.api.item.potion.PotionTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;


public class ValentinesArrowList extends WeightedItemDeterminer {

    private static final String GIFT_LIST_NAME = "Valentines";

    public String giftListName() {
        return GIFT_LIST_NAME;
    }

    // (Yes, I'm fully aware of how lazy and how much of a hack upon a hack this is)
    // No longer

    public ValentinesArrowList() {
        log.info("Initializing the " + giftListName() + " arrow list");

        add(ItemStack.builder()
                .itemType(ItemTypes.TIPPED_ARROW)
                .add(Keys.POTION_TYPE, PotionTypes.HEALING)
                //.add(Keys.DISPLAY_NAME, Text.of(TextColors.DARK_PURPLE, "Arrow of Healing"))
                .build(), 5 * DEFAULT_WEIGHT
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.TIPPED_ARROW)
                .add(Keys.POTION_TYPE, PotionTypes.STRONG_HEALING)
                //.add(Keys.DISPLAY_NAME, Text.of(TextColors.DARK_PURPLE, "Arrow of Super Healing"))
                .build(), 5 * DEFAULT_WEIGHT
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.TIPPED_ARROW)
                .add(Keys.POTION_TYPE, PotionTypes.REGENERATION)
                //.add(Keys.DISPLAY_NAME, Text.of(TextColors.DARK_PURPLE, "Arrow of Regeneration"))
                .build(), 5 * DEFAULT_WEIGHT);

    }
}
