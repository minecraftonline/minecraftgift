package com.minecraftonline.minecraftgift.giftlists;

import com.minecraftonline.minecraftgift.MinecraftGift;
import com.minecraftonline.minecraftgift.WeightedItemDeterminer;
import org.slf4j.Logger;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DirtTypes;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.data.type.PlantTypes;
import org.spongepowered.api.data.type.SandTypes;
import org.spongepowered.api.data.type.SlabTypes;
import org.spongepowered.api.data.type.StoneTypes;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Color;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TerrainGiftList extends WeightedItemDeterminer {

    private static final String GIFT_LIST_NAME = "Terrain";

    private static final Color ORANGE = Color.ofRgb(217, 94, 0);
    private static final Text TERRAIN_EVENT_LORE = Text.of(TextColors.GRAY, "MinecraftOnline Petrifaction Event");

    @Override
    public Text giftHandoutText() {
        return Text.of(TextColors.GREEN, "Hourly ",
                TextColors.GOLD, "Terrain",
                TextColors.GREEN, " random gift handout! Only at MinecraftOnline!");
    }

    public String giftListName() {
        return GIFT_LIST_NAME;
    }

    public TerrainGiftList() {
        final Logger logger = MinecraftGift.getLogger();
        logger.info("Initializing the " + giftListName() + " gift list");

        final HashMap<ItemType, String> armorItems = new HashMap<>();
        armorItems.put(ItemTypes.LEATHER_HELMET, "Cap");
        armorItems.put(ItemTypes.LEATHER_CHESTPLATE, "Chestplate");
        armorItems.put(ItemTypes.LEATHER_LEGGINGS, "Leggings");
        armorItems.put(ItemTypes.LEATHER_BOOTS, "Boots");

        final ItemType[] itemsToColor = {
                ItemTypes.CARPET, ItemTypes.DYE, ItemTypes.BED, ItemTypes.CONCRETE, ItemTypes.CONCRETE_POWDER,
                ItemTypes.STAINED_HARDENED_CLAY, ItemTypes.STAINED_GLASS, ItemTypes.STAINED_GLASS_PANE, ItemTypes.WOOL,
                ItemTypes.BANNER,
        };
        add(ItemTypes.ORANGE_GLAZED_TERRACOTTA);

        for (ItemType i : itemsToColor) {
            add(ItemStack.builder()
                    .itemType(i)
                    .add(Keys.DYE_COLOR, DyeColors.ORANGE)
                    .build());
        }

        for (Map.Entry<ItemType, String> i : armorItems.entrySet()) {
            add(ItemStack.builder()
                    .itemType(i.getKey())
                    .add(Keys.COLOR, ORANGE)
                    .add(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "Petrified ", i.getValue()))
                    .add(Keys.ITEM_LORE, Arrays.asList(TERRAIN_EVENT_LORE))
                    .build());
        }

        add(ItemTypes.ORANGE_SHULKER_BOX, 1);

        add(ItemStack.builder()
                .itemType(ItemTypes.STONE)
                .add(Keys.STONE_TYPE, StoneTypes.SMOOTH_GRANITE)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "Compact Terrain"))
                .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "Heavier than expected"), TERRAIN_EVENT_LORE))
                .build()
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.QUARTZ)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.WHITE, "Nexus Fragment"))
                .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "A shard from a reality beyond our own"), TERRAIN_EVENT_LORE))
                .build()
        );

        add(ItemTypes.STONE);
        add(ItemStack.builder()
                .itemType(ItemTypes.STONE)
                .add(Keys.STONE_TYPE, StoneTypes.GRANITE)
                .build()
        );
        add(ItemStack.builder()
                .itemType(ItemTypes.STONE)
                .add(Keys.STONE_TYPE, StoneTypes.DIORITE)
                .build()
        );
        add(ItemStack.builder()
                .itemType(ItemTypes.STONE)
                .add(Keys.STONE_TYPE, StoneTypes.ANDESITE)
                .build()
        );
        add(ItemTypes.COAL_ORE);
        add(ItemTypes.DIAMOND_ORE);
        add(ItemTypes.EMERALD_ORE);
        add(ItemTypes.GOLD_ORE);
        add(ItemTypes.IRON_ORE);
        add(ItemTypes.LAPIS_ORE);
        add(ItemTypes.REDSTONE_ORE);

        add(ItemTypes.COBBLESTONE);
        add(ItemTypes.COBBLESTONE_WALL);
        add(ItemTypes.STONE_STAIRS);
        add(ItemStack.builder()
                .itemType(ItemTypes.STONE_SLAB)
                .add(Keys.SLAB_TYPE, SlabTypes.COBBLESTONE)
                .build()
        );

        add(ItemTypes.STONE_AXE);
        add(ItemTypes.STONE_SHOVEL);
        add(ItemTypes.STONE_PICKAXE);
        add(ItemTypes.STONE_SWORD);
        add(ItemTypes.STONE_HOE);

        add(ItemTypes.DIRT);
        add(ItemStack.builder()
                .itemType(ItemTypes.DIRT)
                .add(Keys.DIRT_TYPE, DirtTypes.COARSE_DIRT)
                .build()
        );
        add(ItemStack.builder()
                .itemType(ItemTypes.DIRT)
                .add(Keys.DIRT_TYPE, DirtTypes.PODZOL)
                .build()
        );
        add(ItemTypes.GRASS);
        add(ItemTypes.MYCELIUM);

        add(ItemTypes.CLAY);
        add(ItemTypes.GRAVEL);

        add(ItemTypes.SAND, DEFAULT_WEIGHT / 2);
        add(ItemStack.builder()
                .itemType(ItemTypes.SAND)
                .add(Keys.SAND_TYPE, SandTypes.RED)
                .build(),
            DEFAULT_WEIGHT / 2
        );
        
        add(ItemTypes.SANDSTONE, DEFAULT_WEIGHT / 2);
        add(ItemTypes.RED_SANDSTONE, DEFAULT_WEIGHT / 2);
        add(ItemTypes.RED_SANDSTONE_STAIRS);
        add(ItemTypes.STONE_SLAB2);

        add(ItemStack.builder()
                .itemType(ItemTypes.RED_FLOWER)
                .add(Keys.PLANT_TYPE, PlantTypes.ORANGE_TULIP)
                .build()
        );

        add(ItemTypes.POTATO);
        add(ItemTypes.CARROT);
    }

}
