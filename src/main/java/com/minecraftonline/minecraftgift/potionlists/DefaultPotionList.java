package com.minecraftonline.minecraftgift.potionlists;

import com.minecraftonline.minecraftgift.WeightedItemDeterminer;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.potion.PotionType;
import org.spongepowered.api.item.potion.PotionTypes;


public class DefaultPotionList extends WeightedItemDeterminer {

    private static final String GIFT_LIST_NAME = "Default";

    public String giftListName() { return GIFT_LIST_NAME; }


    public DefaultPotionList() {
        log.info("Initializing the " + giftListName() + " potion list");

        // Just to know which we're not using
        PotionType[] avoidPotionTypes = new PotionType[] {
                PotionTypes.EMPTY,
                PotionTypes.WATER,
        };

        // This should, in theory, be every permutation of every potion
        // effect that we actually want to give
        PotionType[] potionTypes = new PotionType[] {
                PotionTypes.MUNDANE,
                PotionTypes.THICK,
                PotionTypes.AWKWARD,

                PotionTypes.STRENGTH,
                PotionTypes.STRONG_STRENGTH,
                PotionTypes.LONG_STRENGTH,

                PotionTypes.WEAKNESS,
                PotionTypes.LONG_WEAKNESS,

                PotionTypes.FIRE_RESISTANCE,
                PotionTypes.LONG_FIRE_RESISTANCE,

                PotionTypes.HARMING,
                PotionTypes.STRONG_HARMING,

                PotionTypes.HEALING,
                PotionTypes.STRONG_HEALING,

                PotionTypes.INVISIBILITY,
                PotionTypes.LONG_INVISIBILITY,

                PotionTypes.LEAPING,
                PotionTypes.STRONG_LEAPING,
                PotionTypes.LONG_LEAPING,

                PotionTypes.SWIFTNESS,
                PotionTypes.STRONG_SWIFTNESS,
                PotionTypes.LONG_SWIFTNESS,

                PotionTypes.SLOWNESS,
                PotionTypes.LONG_SLOWNESS,

                PotionTypes.REGENERATION,
                PotionTypes.STRONG_REGENERATION,
                PotionTypes.LONG_REGENERATION,

                PotionTypes.LONG_POISON,
                PotionTypes.STRONG_POISON,
                PotionTypes.POISON,
        };

        // For every potion of the types we want...
        for (PotionType p : potionTypes) {
            // Add them as a normal potion
            add(ItemStack.builder()
                    .itemType(ItemTypes.POTION)
                    .add(Keys.POTION_TYPE, p)
                    .build()
            );
            // and as a splash potion
            add(ItemStack.builder()
                    .itemType(ItemTypes.SPLASH_POTION)
                    .add(Keys.POTION_TYPE, p)
                    .build()
            );
        }

    }
}
