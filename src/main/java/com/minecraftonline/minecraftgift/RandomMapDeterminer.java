package com.minecraftonline.minecraftgift;

import java.util.ArrayList;
import java.util.Random;

public class RandomMapDeterminer extends MapDeterminer {
    // Declare a random
    private final Random rand = new Random();

    public final int getMapID (ArrayList<Integer> mapIDs) {
        int mapID = mapIDs.get(rand.nextInt(mapIDs.size()));
        return mapID;
    }
}
